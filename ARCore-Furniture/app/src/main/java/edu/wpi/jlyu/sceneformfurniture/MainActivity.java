package edu.wpi.jlyu.sceneformfurniture;

import android.app.AlertDialog;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.google.ar.core.Anchor;
import com.google.ar.core.HitResult;
import com.google.ar.core.Plane;
import com.google.ar.sceneform.AnchorNode;
import com.google.ar.sceneform.FrameTime;
import com.google.ar.sceneform.rendering.ModelRenderable;
import com.google.ar.sceneform.rendering.Renderable;
import com.google.ar.sceneform.ux.ArFragment;
import com.google.ar.sceneform.ux.TransformableNode;

public class MainActivity extends AppCompatActivity {

    private CustomArFragment fragment;
    private Anchor cloudAnchor;

    private enum AppAnchorState {
        NONE,
        HOSTING,
        HOSTED,
        RESOLVING,
        RESOLVED
    }

    private AppAnchorState appAnchorState = AppAnchorState.NONE;

    private StorageManager storageManager;

    private Uri selectedObject;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        fragment = (CustomArFragment) getSupportFragmentManager().findFragmentById(R.id.sceneform_fragment);
        fragment.getPlaneDiscoveryController().hide();  // Hide initial hand gesture
        fragment.getArSceneView().getScene().setOnUpdateListener(this::onUpdateFrame);

        InitializeGallery();

        Button clearButton = findViewById(R.id.clear_button);
        clearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setCloudAnchor(null);
            }
        });

        fragment.setOnTapArPlaneListener(
                (HitResult hitResult, Plane plane, MotionEvent motionEvent) -> {
                     if ( plane.getType() != Plane.Type.HORIZONTAL_UPWARD_FACING
                             || appAnchorState != AppAnchorState.NONE) {
                         return;
                     }

                     Anchor newAnchor = fragment.getArSceneView().getSession().hostCloudAnchor(hitResult.createAnchor());

                     setCloudAnchor(newAnchor);

                     appAnchorState = AppAnchorState.HOSTING;
                     placeObject(fragment, cloudAnchor, selectedObject);
                }
        );

        storageManager = new StorageManager(this);
    }


    private void setCloudAnchor (Anchor newAnchor) {
        if (cloudAnchor != null) {
            cloudAnchor.detach();
        }

        cloudAnchor = newAnchor;
        appAnchorState = AppAnchorState.NONE;
    }

    private void onUpdateFrame(FrameTime frameTime) {
        checkUpdatedAnchor();
    }

    // private synchronized void checkUpdatedAnchor() {
    private void checkUpdatedAnchor() {
        if (appAnchorState != AppAnchorState.HOSTING && appAnchorState != AppAnchorState.RESOLVING) {
            return;
        }
        Anchor.CloudAnchorState cloudState = cloudAnchor.getCloudAnchorState();
        if (appAnchorState == AppAnchorState.HOSTING) {

            if (cloudState.isError()) {

                appAnchorState = AppAnchorState.NONE;
            } else if (cloudState == Anchor.CloudAnchorState.SUCCESS) {
                storageManager.nextShortCode((shortCode) -> {
                    if (shortCode == null) {
                        return;
                    }
                    storageManager.storeUsingShortCode(shortCode, cloudAnchor.getCloudAnchorId());
                });

                appAnchorState = AppAnchorState.HOSTED;
            }
        }
        else if (appAnchorState == AppAnchorState.RESOLVING) {
            if (cloudState.isError()) {
                appAnchorState = AppAnchorState.NONE;
            } else if (cloudState == Anchor.CloudAnchorState.SUCCESS) {
                appAnchorState = AppAnchorState.RESOLVED;
            }
        }
    }

    private void InitializeGallery() {
        LinearLayout gallery = findViewById(R.id.gallery_layout);

        ImageView chair = new ImageView( this );
        chair.setImageResource(R.drawable.chair_thumb);
        chair.setContentDescription("chair");
        chair.setOnClickListener(view -> {selectedObject = Uri.parse("chair.sfb");});
        gallery.addView(chair);

        ImageView lamp = new ImageView( this );
        lamp.setImageResource(R.drawable.lamp_thumb);
        lamp.setContentDescription("lamp");
        lamp.setOnClickListener(view -> {selectedObject = Uri.parse("lamp.sfb");});
        gallery.addView(lamp);

        ImageView sofa = new ImageView( this );
        sofa.setImageResource(R.drawable.sofa_thumb);
        sofa.setContentDescription("sofa");
        sofa.setOnClickListener(view -> {selectedObject = Uri.parse("sofa.sfb");});
        gallery.addView(sofa);

        ImageView table = new ImageView( this );
        table.setImageResource(R.drawable.table_thumb);
        table.setContentDescription("table");
        table.setOnClickListener(view -> {selectedObject = Uri.parse("table.sfb");});
        gallery.addView(table);

        ImageView armchair = new ImageView( this );
        armchair.setImageResource(R.drawable.armchair);
        armchair.setContentDescription("armchair");
        armchair.setOnClickListener(view -> {selectedObject = Uri.parse("Armchair_01.sfb");});
        gallery.addView(armchair);

        ImageView bed = new ImageView( this );
        bed.setImageResource(R.drawable.bed);
        bed.setContentDescription("bed");
        bed.setOnClickListener(view -> {selectedObject = Uri.parse("Bed.sfb");});
        gallery.addView(bed);

        ImageView softwall = new ImageView( this );
        softwall.setImageResource(R.drawable.softwall);
        softwall.setContentDescription("softwall");
        softwall.setOnClickListener(view -> {selectedObject = Uri.parse("Ligne_Roset_Softwall.sfb");});
        gallery.addView(softwall);


        ImageView draw = new ImageView( this );
        draw.setImageResource(R.drawable.draw);
        draw.setContentDescription("draw");
        draw.setOnClickListener(view -> {selectedObject = Uri.parse("model.sfb");});
        gallery.addView(draw);

        ImageView mirror = new ImageView( this );
        mirror.setImageResource(R.drawable.mirror);
        mirror.setContentDescription("mirror");
        mirror.setOnClickListener(view -> {selectedObject = Uri.parse("Dante_Wonderland_Mirror_mat(1).sfb");});
        gallery.addView(mirror);

        ImageView clock = new ImageView( this );
        clock.setImageResource(R.drawable.clock);
        clock.setContentDescription("clock");
        clock.setOnClickListener(view -> {selectedObject = Uri.parse("Clock.sfb");});
        gallery.addView(clock);

        ImageView october = new ImageView( this );
        october.setImageResource(R.drawable.october_painting);
        october.setContentDescription("october");
        october.setOnClickListener(view -> {selectedObject = Uri.parse("october-painting1.sfb");});
        gallery.addView(october);

        ImageView panel = new ImageView( this );
        panel.setImageResource(R.drawable.panel);
        panel.setContentDescription("panel");
        panel.setOnClickListener(view -> {selectedObject = Uri.parse("Obj.sfb");});
        gallery.addView(panel);

        ImageView twin = new ImageView( this );
        twin.setImageResource(R.drawable.wall_light);
        twin.setContentDescription("twin");
        twin.setOnClickListener(view -> {selectedObject = Uri.parse("twin wall lamp.sfb");});
        gallery.addView(twin);

        ImageView walllamp = new ImageView( this );
        walllamp.setImageResource(R.drawable.wall_lamp);
        walllamp.setContentDescription("walllamp");
        walllamp.setOnClickListener(view -> {selectedObject = Uri.parse("Maison_Sarah_Lavoine_Vadim_Wall_Lamp_mat(1).sfb");});
        gallery.addView(walllamp);

        ImageView woman = new ImageView( this );
        woman.setImageResource(R.drawable.woman);
        woman.setContentDescription("woman");
        woman.setOnClickListener(view -> {selectedObject = Uri.parse("objPainting.sfb");});
        gallery.addView(woman);


    }

    // placeObject() takes the position and orientation of an object to be rendered as
    // an anchor and starts async loading the object. Once the builder built the object,
    // addNodeToScene() will be called, and that will actually create the object in
    // the camera view.
    private void placeObject(ArFragment fragment, Anchor anchor, Uri model) {
        ModelRenderable.builder()
                .setSource(fragment.getContext(), model)
                .build()
                .thenAccept(renderable -> addNodeToScene(fragment, anchor, renderable))
                .exceptionally((throwable -> {
                    AlertDialog.Builder builder = new AlertDialog.Builder( this );
                    builder.setMessage(throwable.getMessage())
                            .setTitle("Error!");
                    AlertDialog dialog = builder.create();
                    dialog.show();
                    return null;
                }));
    }

    // Place the object on AR
    private void addNodeToScene(ArFragment fragment, Anchor anchor, Renderable renderable) {
        AnchorNode anchorNode = new AnchorNode(anchor);
        TransformableNode node = new TransformableNode(fragment.getTransformationSystem());
        node.setRenderable(renderable);
        node.setParent(anchorNode);
        fragment.getArSceneView().getScene().addChild(anchorNode);
        node.select();
    }

}
